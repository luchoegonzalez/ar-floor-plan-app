using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Experimental.GlobalIllumination;

public class ScaleSlider : MonoBehaviour
{
	public Slider mainSlider2;
	public GameObject model;
	
	public void Start()
	{
		//Adds a listener to the main slider and invokes a method when the value changes.
		mainSlider2.onValueChanged.AddListener (delegate {ValueChangeCheck2 ();});
	}
	
	// Invoked when the value of the slider changes.
	public void ValueChangeCheck2()
	{
		model.transform.localScale = new Vector3(mainSlider2.value*3 + 1, mainSlider2.value*3 + 1, mainSlider2.value*3 + 1);
	}
    // Update is called once per frame
    void Update()
    {
        
    }
}
