using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Experimental.GlobalIllumination;

public class SliderUpdate : MonoBehaviour
{
	public Slider mainSlider;
	public GameObject luz;
	
	public void Start()
	{
		//Adds a listener to the main slider and invokes a method when the value changes.
		mainSlider.onValueChanged.AddListener (delegate {ValueChangeCheck ();});
	}
	
	// Invoked when the value of the slider changes.
	public void ValueChangeCheck()
	{
		luz.transform.localRotation = Quaternion.Euler (mainSlider.value*180, -30, 0);
	}
    // Update is called once per frame
    void Update()
    {
        
    }
}
